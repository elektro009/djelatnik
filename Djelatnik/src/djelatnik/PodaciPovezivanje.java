/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package djelatnik;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author ljubomir
 */
public class PodaciPovezivanje {
    String Host,Port,Dbname,file="podaci.txt";
    public PodaciPovezivanje()
    {}
    public PodaciPovezivanje(String host, String port, String dbname) throws IOException
    {
        Host=host;
        Port=port;
        Dbname=dbname;
    }
    public boolean Zapisi(String host, String port, String dbname) throws IOException
    {
            FileWriter fileWriter =new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(host+"\n",0,host.length()+1);
            bufferedWriter.write(port+"\n",0,port.length()+1);
            bufferedWriter.write(dbname+"\n",0,dbname.length()+1);
            bufferedWriter.close();
        return true;
    }
    public boolean Procitaj() throws FileNotFoundException, IOException
    {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        if((Host=bufferedReader.readLine())==null) return false;
        if((Port=bufferedReader.readLine())==null) return false;
        if((Dbname=bufferedReader.readLine())==null) return false;
        bufferedReader.close();
        return true;
    }
}
