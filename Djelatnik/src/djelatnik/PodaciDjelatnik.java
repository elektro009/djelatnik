/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package djelatnik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ljubomir
 */
public class PodaciDjelatnik {
    Connection conn;
    String server_podaci;
    public PodaciDjelatnik()
    {}
    public PodaciDjelatnik(String host, String port, String dbname)
    {
        server_podaci="jdbc:sqlserver:"+host+":"+port+";databaseName="+dbname;
    }
    public boolean Povezi(String username, String password)
    {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(server_podaci, username, password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PodaciDjelatnik.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PodaciDjelatnik.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    public ResultSet Select(String query) throws SQLException, ClassNotFoundException
    {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(query);
        return rs;
    }
    public void Insert(String query) throws ClassNotFoundException, SQLException
    {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Statement statement = conn.createStatement();
        statement.execute(query);
    }
    public boolean Delete(String query) throws ClassNotFoundException, SQLException
    {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Statement statement = conn.createStatement();
        return statement.execute(query);
    }
    
}
